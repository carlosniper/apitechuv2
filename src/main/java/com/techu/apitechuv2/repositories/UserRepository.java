package com.techu.apitechuv2.repositories;

import com.techu.apitechuv2.models.UserModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends MongoRepository<UserModel, String> {

    List<UserModel> findByOrderByAgeAsc();
}
