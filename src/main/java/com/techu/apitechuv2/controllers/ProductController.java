package com.techu.apitechuv2.controllers;

import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts() {
        System.out.println("getProduct");

        return new ResponseEntity<>(this.productService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/products/{id}")
    public ResponseEntity getProductById(@PathVariable String id) {
        System.out.println("getProductById");

        Optional<ProductModel> opProductModel = this.productService.findById(id);

        if(opProductModel.isEmpty()) {
            return new ResponseEntity(String.format("El usuario con id %s no existe", id), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(opProductModel.get(), HttpStatus.OK);
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel productModel){
        System.out.println("addProduct");

        return new ResponseEntity<>(this.productService.save(productModel), HttpStatus.CREATED);

    }

    @PutMapping("/products/{id}")
    public ResponseEntity updateProduct(@PathVariable String id, @RequestBody ProductModel productModel) {
        System.out.println("updateProduct");

        Optional<ProductModel> response = this.productService.update(id, productModel);

        if (response.isPresent()) { return new ResponseEntity<>(response.get(), HttpStatus.OK); }
        return new ResponseEntity<>(String.format("El producto con id %s no existe", id), HttpStatus.NOT_FOUND);

    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct");

        if(this.productService.deleteById(id)) {
            return new ResponseEntity(String.format("Producto con id %s borrado correctamente", id), HttpStatus.OK);
        }
        return new ResponseEntity(String.format("El producto con id %s no existe", id), HttpStatus.NOT_FOUND);
    }
}
