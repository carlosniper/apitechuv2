package com.techu.apitechuv2.controllers;

import com.techu.apitechuv2.models.UserModel;
import com.techu.apitechuv2.repositories.UserRepository;
import com.techu.apitechuv2.services.UserService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> findAll(@RequestParam(name = "orderBy", required = false) String orderBy) {
        System.out.println("findAll");

        return new ResponseEntity<>(this.userService.findAll(orderBy), HttpStatus.OK);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity findById(@PathVariable String id) {
        System.out.println("findById");

        Optional<UserModel> oUserModel = this.userService.findById(id);

        if(oUserModel.isPresent()) {
            return new ResponseEntity<>(oUserModel.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(String.format("El usuario con id %s no existe", id), HttpStatus.NOT_FOUND);
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> create(@RequestBody UserModel user) {
        System.out.println("create");

        return new ResponseEntity<>(this.userService.save(user), HttpStatus.CREATED);
    }

    @PutMapping("users/{id}")
    public ResponseEntity update(@PathVariable String id, @RequestBody UserModel user) {
        System.out.println("update");

        Optional<UserModel> oUserModel = this.userService.update(id, user);

        if(oUserModel.isPresent()) {
            return new ResponseEntity<>(oUserModel.get(), HttpStatus.OK);
        }
        return new ResponseEntity(String.format("El usuario con id %s no existe", id), HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("users/{id}")
    public ResponseEntity delete(@PathVariable String id) {
        System.out.println("delete");

        if (this.userService.delete(id)) {
            return new ResponseEntity(HttpStatus.OK);
        }
        return new ResponseEntity(String.format("El usuario con id %s no existe", id), HttpStatus.NOT_FOUND);
    }
}

