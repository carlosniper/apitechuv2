package com.techu.apitechuv2.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

@RestController
public class HelloController {

    @RequestMapping("/")
    public ResponseEntity<String> index() {
        return new ResponseEntity<>("Hola desde apitechu v2", HttpStatus.OK);
    }

    @GetMapping("/greetings")
    public ResponseEntity<String> greetings(@RequestParam (defaultValue = "API Tech U") String name) {
        return new ResponseEntity<>(String.format("Hola %s!", name), HttpStatus.OK);
    }

}
