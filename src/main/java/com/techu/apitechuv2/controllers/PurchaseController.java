package com.techu.apitechuv2.controllers;

import com.techu.apitechuv2.models.PurchaseModel;
import com.techu.apitechuv2.models.PurchaseResponse;
import com.techu.apitechuv2.services.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/apitechu/v2/purchases/")
public class PurchaseController {

    @Autowired
    private PurchaseService purchaseService;

    @PostMapping("")
    public ResponseEntity<PurchaseResponse> addPurchase(@RequestBody PurchaseModel purchase) {
        PurchaseResponse response = this.purchaseService.addPurchase(purchase);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

}
