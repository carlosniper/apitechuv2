package com.techu.apitechuv2.models;

import org.springframework.http.HttpStatus;

public class PurchaseResponse {

    private String msg;
    private PurchaseModel purchase;
    private HttpStatus httpStatus;

    public PurchaseResponse() {

    }

    public PurchaseResponse(String msg, PurchaseModel purchase, HttpStatus httpStatus) {
        this.msg = msg;
        this.purchase = purchase;
        this.httpStatus = httpStatus;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public PurchaseModel getPurchase() {
        return this.purchase;
    }

    public void setPurchase(PurchaseModel purchase) {
        this.purchase = purchase;
    }

    public HttpStatus getHttpStatus() {
        return this.httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}
