package com.techu.apitechuv2.services;

import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.models.PurchaseModel;
import com.techu.apitechuv2.models.PurchaseResponse;
import com.techu.apitechuv2.repositories.ProductRepository;
import com.techu.apitechuv2.repositories.PurchaseRepository;
import com.techu.apitechuv2.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    private PurchaseRepository purchaseRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductRepository productRepository;

    public PurchaseResponse addPurchase(PurchaseModel purchase) {
        PurchaseResponse response = new PurchaseResponse();
        if(Objects.isNull(purchase)) {
            response.setMsg("PurchaseModel is mandatory in request body!");
            response.setHttpStatus(HttpStatus.BAD_REQUEST);
            return response;
        }
        if(this.userRepository.findById(purchase.getUserId()).isEmpty()) {
            response.setMsg(String.format("User with id %s not found!", purchase.getUserId()));
            response.setHttpStatus(HttpStatus.NOT_FOUND);
            return response;
        }
        if(this.purchaseRepository.findById(purchase.getId()).isPresent() == true) {
            response.setMsg(String.format("A purchase with id %s is already created!", purchase.getId()));
            response.setHttpStatus(HttpStatus.BAD_REQUEST);
        }
        float amount = 0;
        for (Map.Entry<String, Integer> purchaseItem : purchase.getPurchaseItems().entrySet()) {
            Optional<ProductModel> oProductModel = this.productRepository.findById(purchaseItem.getKey());
            if(oProductModel.isEmpty()) {
                response.setMsg(String.format("Product with id %s not found!", purchaseItem.getKey()));
                response.setHttpStatus(HttpStatus.NOT_FOUND);
            } else {
                amount += oProductModel.get().getPrice() * purchaseItem.getValue();
            }
        }
        purchase.setAmount(amount);
        PurchaseModel purchaseModel = this.purchaseRepository.save(purchase);
        response.setMsg("Purchase saved correctly");
        response.setHttpStatus(HttpStatus.CREATED);
        response.setPurchase(purchaseModel);
        return response;
    }
}
