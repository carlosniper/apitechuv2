package com.techu.apitechuv2.services;

import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.models.UserModel;
import com.techu.apitechuv2.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<UserModel> findAll(String orderBy) {

        if (Objects.nonNull(orderBy)) {
            return this.userRepository.findByOrderByAgeAsc();
        }
        return this.userRepository.findAll();
    }

    public Optional<UserModel> findById(String id) {
        return this.userRepository.findById(id);
    }

    public UserModel save(UserModel user) {
        return this.userRepository.save(user);
    }

    public Optional<UserModel> update(String id, UserModel user) {
        Optional<UserModel> opUserModel = this.userRepository.findById(id);

        if(opUserModel.isPresent()) {
            UserModel umDb = opUserModel.get();
            umDb.setName(user.getName());
            umDb.setAge(user.getAge());
            return Optional.of(this.userRepository.save(umDb));
        }
        return Optional.empty();
    }

    public boolean delete(String id) {
        Optional<UserModel> opUserModel = this.userRepository.findById(id);

        if(opUserModel.isPresent()) {
            this.userRepository.deleteById(id);
            return true;
        }
        return false;
    }
}
