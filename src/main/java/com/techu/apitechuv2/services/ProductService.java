package com.techu.apitechuv2.services;

import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll() {
        return this.productRepository.findAll();
    }

    public Optional<ProductModel> findById(String id) {
        return this.productRepository.findById(id);
    }

    public ProductModel save(ProductModel productModel) {
        return this.productRepository.save(productModel);
    }

    public Optional<ProductModel> update(String id, ProductModel productModel) {

        Optional<ProductModel> opProductModel = this.productRepository.findById(id);

        if(opProductModel.isPresent()) {
            ProductModel pmDb = opProductModel.get();
            pmDb.setDesc(productModel.getDesc());
            pmDb.setPrice(productModel.getPrice());
            return Optional.of(this.productRepository.save(pmDb));
        }
        return Optional.empty();
    }

    public boolean deleteById(String id) {

        Optional<ProductModel> opProductModel = this.productRepository.findById(id);

        if(opProductModel.isPresent()) {
            this.productRepository.deleteById(id);
            return true;
        }

        return false;
    }
}
