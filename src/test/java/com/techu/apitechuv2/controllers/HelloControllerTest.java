package com.techu.apitechuv2.controllers;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class HelloControllerTest {

    @Test
    public void testHello() {
        HelloController sut = new HelloController();

        ResponseEntity<String> response = sut.greetings("carlos");
        Assert.assertEquals(response.getBody(), "Hola carlos!");
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
    }
}
