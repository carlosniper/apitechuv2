package com.techu.apitechuv2.services;

import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.repositories.ProductRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private ProductService productService;

    @Test
    public void findAll_ok() {
        List<ProductModel> list_product = new ArrayList<>();
        list_product.add(new ProductModel("1", "Producto 1", 29.99f));
        when(this.productRepository.findAll()).thenReturn(list_product);

        List<ProductModel> result = this.productService.findAll();

        Assert.assertFalse(result.isEmpty());
        Assert.assertEquals(result.get(0).getId(), "1");
        Assert.assertEquals(result.get(0).getDesc(), "Producto 1");
        Assert.assertEquals(Float.valueOf(result.get(0).getPrice()), Float.valueOf(29.99f));
    }

}
